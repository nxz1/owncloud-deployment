# Owncloud Docker Deployment

###### Prerequisites: docker and docker-compose installed.

### How to start the app ?
  - cd docker
  - docker-compose up -d

### Access Owncloud
  Access owncloud server at http://localhost/

### Initial Credentials
  - Owncloud Admin User: admin
  - Owncloud Admin Pass: admin