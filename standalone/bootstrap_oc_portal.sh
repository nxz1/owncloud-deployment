#!/bin/bash

### Enable Owncloud Repo
sudo rpm --import http://download.owncloud.org/download/repositories/9.1/CentOS_7/repodata/repomd.xml.key
wget http://download.owncloud.org/download/repositories/9.1/CentOS_7/ce:9.1.repo
mv ce:9.1.repo /etc/yum.repos.d/

### Install Apache 2
yum install owncloud -y

### Start / Enable Apache Service
systemctl enable --now httpd mariadb

### Disable system firewall
systemctl disable --now firewalld iptables ip6tables

### Configure DB Root Password
MYSQL_ROOT_PASSWORD="Owncloud@Root_1234"
DATABASE_NAME="owncloud_prod"
DATABASE_HOST="localhost"
OC_ADMIN_DB_USER="prod"
OC_ADMIN_DB_PASSWORD="Prod@123"


# Set the MySQL root password
echo "Setting MySQL root password..."
mysqladmin -u root password "${MYSQL_ROOT_PASSWORD}"
echo "MySQL root password set."

### Configure DB for NextCloud / Owncloud
mysql -uroot --password=$MYSQL_ROOT_PASSWORD -e "CREATE DATABASE $DATABASE_NAME;"
mysql -uroot --password=$MYSQL_ROOT_PASSWORD -e "CREATE USER '$OC_ADMIN_DB_USER'@'$DATABASE_HOST' IDENTIFIED BY '$OC_ADMIN_DB_PASSWORD';"
mysql -uroot --password=$MYSQL_ROOT_PASSWORD -e "GRANT ALL ON $DATABASE_NAME.* to '$OC_ADMIN_DB_USER'@'$DATABASE_HOST';"
mysql -uroot --password=$MYSQL_ROOT_PASSWORD -e "FLUSH PRIVILEGES;"
