# Owncloud Server Deployment

###### Prerequisites: vagrant installed.

### How to start the app ?
  - cd standalone
  - vagrant up 

### Access Owncloud
  Access owncloud server at http://localhost/
